<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['guest']], function () {
    Route::post('/login', 'Api\AuthController@login')->name('login');
    Route::post('/register', 'Api\AuthController@Register');
});


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/beers', 'Api\BeerController@GetBeers');
    Route::post('/user/update', 'Api\UserController@update');
    Route::get('/user', 'Api\UserController@show');
    Route::get('/logout', 'Api\AuthController@logout')->name('logout');
});
