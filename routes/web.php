<?php

/*
|--------------------------------------------------------------------------
| Laravel Passport Routes
|--------------------------------------------------------------------------
|
| If you'd like to use full functionality of laravel passport
| use the routes below instead of registering your routes in
| AuthServiceProvider as instructed in passport documentation.
| This will use "auth:api" guard instead of "auth"
|
*/



/*
|--------------------------------------------------------------------------
| Vue Router
|--------------------------------------------------------------------------
|
| Allows vue router to pick up all other URL's.
|
*/
Route::any('{all}', function(){
    return view('app');
})->where(['all' => '.*']);
