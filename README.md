# Teste Vaga Full Stack TLO

Demo: [https://beerblioteca.herokuapp.com/](https://beerblioteca.herokuapp.com/) 

### O teste
Criar uma aplicação utilizando as tecnologias que você mais gostar e uma interface web de login e consulta de dados de uma API externa

### O projeto
- O front-end deve ser simples e deve apresentar funcionalidades básicas como login, mensagens de usuário / senha incorretos, ...
- Depois de logado o usuário da aplicação deve poder consumir, pela interface, dados de uma das API's externas *(da escolha do candidato)* da listagem de [API's públicas](https://github.com/toddmotto/public-apis).
  - Uma das API's entrega [cervejas e não necessita autenticação](https://punkapi.com/)
  - Deve se apresentar os dados numa listagem simples com paginação e, opcionalmente, filtros
  - Os endpoints de consulta de dados devem todos ter autenticação por webtoken ou similar
  - Será julgado, além do funcionamento e estrutura do código, o uso de boas práticas, segurança e produtividade de código e a compreensão do teste.

### Entregas
Deve ser entregue um repositório contendo o projeto
  
### Opcional
Publicação do projeto (pode-se utilizar hospedagens gratuitas como AWS, Heroku, ...)

### Atenção:
Se você não conseguir completar todo o teste, fique tranquilo e mostre tudo que você conseguiu fazer 

## Tecnologias usadas:  
Laravel  
Vue/Vuex  
Bulma  

## Instalação
 1. Clone este repositório  
 ` git clone https://gitlab.com/cilas/teste-vaga-full-stack-tlo.git`  
 `cd teste-vaga-full-stack-tlo`  
 2. Instale as dependencias  
 `composer install`  
 `yarn`  
 3. Configure o ambiente  
 `cp .env.example .env`  
 `php artisan key:generate`  
    - Configure o banco de dados de sua preferência  
 `php artisan migrate`  
 `php artisan passport:install`  
    - Copie o client id e client secret e cole nas chaves `CLIENT_ID` E `CLIENT_SECRET` do seu arquivo `.env`  
 4. Rode o servidor  
 `php artisan serve`  

## API
- Registrando-se no site  
Uri: https://beerblioteca.herokuapp.com/api/register  
Tipo: POST  
Campos: `name`, `email`, `password` e `password_confirmation`  
Autenticação: Não é necessario  

- Login no site  
Uri: https://beerblioteca.herokuapp.com/api/login  
Tipo: POST  
Campos: `email`, `password` e `remember_me` (opcional)  
Authorization: Bearer Token  
- Pegar dados do usuário  
Uri: https://beerblioteca.herokuapp.com/api/user/  
Tipo: GET  
Authorization: Bearer Token  
- Editando o Usuário  
Uri: https://beerblioteca.herokuapp.com/api/user/update  
Tipo: POST  
Campos: `name`, `cpf`, `birth_day`, `genre`, `phone`  
Obs: Os campos `password`, `email`, `email_verified_at`, `created_at`, `updated_at`, não serão alterados.  
Authorization: Bearer Token  

