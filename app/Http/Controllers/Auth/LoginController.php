<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
class LoginController extends Controller
{
    public function login (Request $request) {
            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken(config('services.spa_client.secret'))->accessToken;
                    $response = ['access_token' => $token];
                    return response($response, 200);
                } else {
                    $response = "invalid_password";
                    return response($response, 422);
                }
            } else {
                $response = 'user_not_found';
                return response($response, 422);
            }
        }

    public function logout (Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = 'You have been succesfully logged out!';
    }
}
