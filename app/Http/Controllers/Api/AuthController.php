<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use Validator;
class AuthController extends Controller
{
    public function Register(Request $request){

         $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'password_confirmation'=>'required|same:password'
        ]);

        if ($validator->fails()) {
          return response()->json($validator->errors(), 433);
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        if ($user->save()) {
          return response()->json([['message'=> 'Cadastro realizado com sucesso!'], $user], 200);
        }
    }

    public function login (Request $request) {

            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string',
                'remember_me' => 'boolean'
            ]);
            $credentials = request(['email', 'password']);
            if(!Auth::attempt($credentials))
                return response("Invalid_credentials", 422);
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        }

    public function logout (Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = 'Você foi desconectado com sucesso!';
        return response()->json($response, 200);
    }
}
