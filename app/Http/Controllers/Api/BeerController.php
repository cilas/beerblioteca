<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class BeerController extends Controller
{
    private function ConfigGuzzle()
    {
        $client = new Client(['base_uri' => 'https://api.punkapi.com/v2/']);
        return $client;
    }

    public function GetBeers(Request $request)
    {
        $client = $this->ConfigGuzzle();
        try {
            $beer_request = $client->request('GET', 'beers', [
                'query' => $request->all()
            ]);
        } catch (ClientException $e) {
            return Psr7\str($e->getResponse());
        }

        $response = $beer_request->getBody();
        return json_decode($response);
    }

    public function ShowBeer($id)
    {
        $client = $this->ConfigGuzzle();
        $request = $client->request('GET', 'beers/'.$id);
        $response = $request->getBody();
        return json_decode($response);
    }


}
