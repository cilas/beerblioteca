<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;

class UserController extends Controller
{

    public function show(Request $request) {
        return $request->user();
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'phone'=>  'nullable|max:20',
            'cpf'=>'nullable|min:11|max:11',
            'genre' =>'nullable|max:1',
            'birth_day'=> 'nullable|date',
        ]);
        if ($validator->fails())
            return response($validator->errors(), 433);
        $user = $request->user();
        $user->update($request->except(['email', 'email_verified_at', 'created_at', 'updated_at', 'password']));
        return response()->json($user);
    }
}
