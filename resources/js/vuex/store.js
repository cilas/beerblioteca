import Vue from 'vue';
import Vuex from 'vuex';

import user from './modules/user'
import auth from './modules/auth'
import beer from './modules/beer'

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        user,
        auth,
        beer
    }
});
