import Vue from 'vue'

const state = {
    status: '',
    beers: [],
    loading: false,
    total: 0,

}

const getters = {
    getBeers: state => state.beers,
    getLoading: state => state.loading
}

const actions = {

    beerRequest: ({
        commit,
    }, params) => {
        commit('beerRequest')
        axios.get(`/api/beers?${params}`)
            .then((resp) => {
                commit('beerSuccess', resp.data);
            })
            .catch((err) => {
                commit('beerError');
            })
    }
}

const mutations = {
    beerRequest: (state) => {
        state.status = 'carregando';
        state.loading = true
    },
    beerSuccess: (state, resp) => {
        state.status = 'success';
        Vue.set(state, 'beers', resp);
        state.loading = false
    },
    changePage:(state, page)=>{
        state.currentPage = page
    },
    beerError: (state) => {
        state.status = 'error';
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
