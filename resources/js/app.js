/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
import Buefy from 'buefy'
Vue.use(Buefy)
import {
    router
} from './router/index';

import {
    store
} from './vuex/store';

const app = new Vue({
    el: '#app',
    created() {
        if (this.$store.getters.isAuthenticated) {
            this.$store.dispatch('userRequest');
        }
    },
    store,
    router: router
});
