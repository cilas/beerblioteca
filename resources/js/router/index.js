import Vue from 'vue';
import VueRouter from 'vue-router'
import {
    store
} from '../vuex/store'

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/dashboard')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
}

import Login from './../components/auth/login'
import Layout from './../components/layouts/layout'
import Dashboard from './../components/dashboard.vue'

const routes = [{
        path: '/',
        name: 'login',
        alias: '/login',
        component: Login,
        beforeEnter: ifNotAuthenticated,
    },
    {
        path: '/',
        name: 'layout',
        component: Layout,
        beforeEnter: ifAuthenticated,
        children: [{
            path: 'dashboard',
            name: 'dashboard',
            component: Dashboard
        }]
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes
});
